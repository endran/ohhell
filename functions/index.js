const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();
const firestore = admin.firestore();

exports.helloWorld = functions.https.onCall((data, context) => {
  return {
    test: 'responded',
  };
});

exports.dealHands = functions.https.onCall((data, context) => {
  console.log( data.text )
  functions.logger.log( data.text )
  // firestore.collection('game').add({ app: 'noot' })
});

exports.startGame = functions.https.onCall((data, context) => {
  // data.gameid
  // functions.logger.log( result.data().state );
  
  const gameRef = firestore.collection('game').doc(data.gameid);
  const playersRef = gameRef.collection('player');
  const cardsRef = gameRef.collection('cards').where('location', '==', 'deck');

  const batch = firestore.batch();

  var game, players, cards;

  gameRef.get()
  .then( (result) => {
    game = result;
    if ( game.data().state != 'start' ) { return; }
    return playersRef.get();
  } )
  .then( (result) => {
    players = result;
    return cardsRef.get();
  } )
  .then( (result) => {
    cards = result;
    /*
    batch.update( gameR.ref, {
      round: gameR.data().round+1
    } )
    */
    const round = game.data().round;
    var card;
    players.forEach( doc => {
      functions.logger.log('Dealing cards for '+doc.data().name);
      for ( var i=0; i<10; i++ ) {
        card = cards[i];
        functions.logger.log(card);
        /*
        batch.update( game.collection('cards').doc(card.id), {
          location: 'hand',
        });
        */
      }
    });
    return batch.commit()
  } );

});

exports.createGame = functions.firestore
  .document('/game/{gameId}')
  .onCreate(( snap, context ) => {
    snap.ref.set({
      name: snap.data().name || 'game name',
      state: 'start',
      round: 1,
    });
    // 0-15 in 6 colors
    for ( var color of ['blue', 'green', 'yellow', 'red', 'violet', 'orange'])  {
      for ( var value=0; value<15; value++)  {
        snap.ref.collection('cards').add({
          color: color,
          value: value,
          location: 'deck',
        });
      }
    }
    // 2 jokers
    for ( var i=0; i<2; i++ ) {
      snap.ref.collection('cards').add({ color: 'grey', value: 'joker', location: 'deck' });
    }
    // 4 keer no-trump, 4 keer new-trump
    for ( var i=0; i<4; i++ ) {
      snap.ref.collection('cards').add({ color: 'grey', value: 'no-trump', location: 'deck' });
      snap.ref.collection('cards').add({ color: 'grey', value: 'new-trump', location: 'deck' });
    }
    // 3 keer +5, 3 keer -5
    for ( var i=0; i<3; i++ ) {
      snap.ref.collection('cards').add({ color: 'grey', value: '-5', location: 'deck' });
      snap.ref.collection('cards').add({ color: 'grey', value: '+5', location: 'deck' });
    }
    return snap.ref;
});
