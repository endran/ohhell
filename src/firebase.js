import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/functions'

firebase.initializeApp({ projectId: 'ohhell-27014' })

const db = firebase.firestore()
const fun = firebase.functions()

if (location.hostname === "localhost") {
  db
    .settings({
      host: 'localhost:8080',
      ssl: false,
    })

  fun
    .useFunctionsEmulator(
      'http://localhost:5001'
    )
}

export { db, fun }
